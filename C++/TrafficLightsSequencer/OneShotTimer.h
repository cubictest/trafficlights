#pragma once

#include <functional>

class OneShotTimer
{
public:
    virtual ~OneShotTimer() {}
    virtual void Schedule(int timeoutInSeconds, std::function<void()>) = 0;
};
