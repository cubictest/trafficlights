#pragma once
#include "Light.h"
#include "OneShotTimer.h"

class TrafficLightsSequencer
{
public:
    static const int TwoMinutes = 120;
    static const int TwoSeconds = 2;

    TrafficLightsSequencer(Light& redLight, Light& amberLight, Light& greenLight, OneShotTimer& oneShotTimer);
    ~TrafficLightsSequencer();
    void Run();

private:
    void PrepareToGo();
    void Go();
    void PrepareToStop();
    void Stop();

    Light& redLight;
    Light& amberLight;
    Light& greenLight;
    OneShotTimer& oneShotTimer;
};

