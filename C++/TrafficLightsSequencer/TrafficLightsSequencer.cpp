#include "TrafficLightsSequencer.h"

TrafficLightsSequencer::TrafficLightsSequencer(
    Light& newRedLight, 
    Light& newAmberLight, 
    Light& newGreenLight,
    OneShotTimer& newOneShotTimer) :
    redLight(newRedLight), 
    amberLight(newAmberLight), 
    greenLight(newGreenLight),
    oneShotTimer(newOneShotTimer)
{
}

void TrafficLightsSequencer::Run()
{
    Stop();
}

void TrafficLightsSequencer::PrepareToGo()
{
    amberLight.SwitchOn();

    oneShotTimer.Schedule(TwoSeconds, std::bind(&TrafficLightsSequencer::Go, this));
}

void TrafficLightsSequencer::Go()
{
    redLight.SwitchOff();
    amberLight.SwitchOff();
    greenLight.SwitchOn();

    oneShotTimer.Schedule(TwoMinutes, std::bind(&TrafficLightsSequencer::PrepareToStop, this));
}

void TrafficLightsSequencer::PrepareToStop()
{
    amberLight.SwitchOn();
    greenLight.SwitchOff();

    oneShotTimer.Schedule(TwoSeconds, std::bind(&TrafficLightsSequencer::Stop, this));
}

void TrafficLightsSequencer::Stop()
{
    redLight.SwitchOn();
    amberLight.SwitchOff();
    greenLight.SwitchOff();

    oneShotTimer.Schedule(TwoMinutes, std::bind(&TrafficLightsSequencer::PrepareToGo, this));
}

TrafficLightsSequencer::~TrafficLightsSequencer()
{
}

