#pragma once
class Light
{
public:
    virtual ~Light() {}
    virtual void SwitchOn() = 0;
    virtual void SwitchOff() = 0;
};
