#pragma once

#include "gmock\gmock.h"
#include "Light.h"

#include <vector>

using ::testing::Invoke;

class LightMock : public Light {
private:
    std::vector<bool> lightStates;

public:
    MOCK_METHOD0(SwitchOn, void());
    MOCK_METHOD0(SwitchOff, void());
    void RegisterSwitchOff() { lightStates.push_back(false); }
    void RegisterSwitchOn() { lightStates.push_back(true); }
    bool IsLastCallSwitchOff() { return !lightStates.empty() && lightStates.back() == false; }
    bool IsLastCallSwitchOn() { return !lightStates.empty() && lightStates.back() == true; }

    LightMock() 
    { 
        ON_CALL(*this, SwitchOff()).WillByDefault(Invoke(this, &LightMock::RegisterSwitchOff));
        ON_CALL(*this, SwitchOn()).WillByDefault(Invoke(this, &LightMock::RegisterSwitchOn));
    }
};
