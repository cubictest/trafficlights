#pragma once

#include "OneShotTimer.h"

#include "gmock\gmock.h"

#include <functional>

class OneShotTimerMock : public OneShotTimer 
{
public:
    MOCK_METHOD2(Schedule, void(int, std::function<void()>));
};

