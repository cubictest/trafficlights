#include "TrafficLightsSequencerTestFixture.h"

#include <gmock/gmock.h>

#include <string>

using ::testing::InSequence;
using ::testing::InvokeArgument;
using ::testing::_;
using ::testing::AtLeast;

TrafficLightsSequencerTestFixture::TrafficLightsSequencerTestFixture() : sequencer(redLightMock, amberLightMock, greenLightMock, oneShotTimerMock)
{
}

void TrafficLightsSequencerTestFixture::AssertLightsInStopState()
{
    ASSERT_TRUE(redLightMock.IsLastCallSwitchOn());
    ASSERT_TRUE(amberLightMock.IsLastCallSwitchOff());
    ASSERT_TRUE(greenLightMock.IsLastCallSwitchOff());
}

void TrafficLightsSequencerTestFixture::AssertLightsInPrepareToGoState()
{
    ASSERT_TRUE(redLightMock.IsLastCallSwitchOn());
    ASSERT_TRUE(amberLightMock.IsLastCallSwitchOn());
    ASSERT_TRUE(greenLightMock.IsLastCallSwitchOff());
}

void TrafficLightsSequencerTestFixture::AssertLightsInGoState()
{
    ASSERT_TRUE(redLightMock.IsLastCallSwitchOff());
    ASSERT_TRUE(amberLightMock.IsLastCallSwitchOff());
    ASSERT_TRUE(greenLightMock.IsLastCallSwitchOn());
}

void TrafficLightsSequencerTestFixture::AssertLightsInPrepareToStopState()
{
    ASSERT_TRUE(redLightMock.IsLastCallSwitchOff());
    ASSERT_TRUE(amberLightMock.IsLastCallSwitchOn());
    ASSERT_TRUE(greenLightMock.IsLastCallSwitchOff());
}

void TrafficLightsSequencerTestFixture::ExpectTimerScheduled(int timeoutInSeconds)
{
    EXPECT_CALL(oneShotTimerMock, Schedule(timeoutInSeconds, _));
}

void TrafficLightsSequencerTestFixture::ExpectTimerScheduledAndInvokeCallback(int timeoutInSeconds)
{
    EXPECT_CALL(oneShotTimerMock, Schedule(timeoutInSeconds, _)).WillOnce(InvokeArgument<1>());
}

void TrafficLightsSequencerTestFixture::ExpectListOfScheduledTimeoutsAndTriggerInvoke(std::vector<int> scheduledTimeouts)
{
    InSequence sequence;

    std::vector<int>::iterator lastItem = std::prev(scheduledTimeouts.end());

    for (std::vector<int>::iterator scheduledTimeoutIterator = scheduledTimeouts.begin();
        scheduledTimeoutIterator != lastItem;
        ++scheduledTimeoutIterator)
    {
        ExpectTimerScheduledAndInvokeCallback(*scheduledTimeoutIterator);
    }

    ExpectTimerScheduled(*lastItem);
}
