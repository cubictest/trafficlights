#include <string>
#include <vector>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "TrafficLightsSequencer.h"
using ::testing::Return;

class LightMock : public Light {
public:

    MOCK_METHOD0(SwitchOn, void());
};

TEST(HtmlParser, NoData) {
    LightMock redLightMock;
    TrafficLightsSequencer sequencer(redLightMock);
//    EXPECT_CALL(mock, getUrlAsString("http://example.net"))
  //      .WillOnce(Return(std::string(html)));
  //  std::vector<std::string> links = parser.getAllLinks("http://example.net");
   // EXPECT_EQ(0, links.size());
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}