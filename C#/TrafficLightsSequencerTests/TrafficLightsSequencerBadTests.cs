﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TrafficLightsSequencer;
using System.Collections.Generic;

namespace TrafficLightsSequencerTests
{
    [TestClass]
    public class TrafficLightsSequencerBadTests
    {
        [TestMethod]
        public void GoSequenceTest()
        {
            Mock<Light> redLight = new Mock<Light>();
            List<string> redCalls = new List<string>();
            redLight.Setup(m => m.SwitchOn()).Callback(() => redCalls.Add("SwitchOn"));
            redLight.Setup(m => m.SwitchOff()).Callback(() => redCalls.Add("SwitchOff"));
            Mock<Light> amberLight = new Mock<Light>();
            List<string> amberCalls = new List<string>();
            amberLight.Setup(m => m.SwitchOn()).Callback(() => amberCalls.Add("SwitchOn"));
            amberLight.Setup(m => m.SwitchOff()).Callback(() => amberCalls.Add("SwitchOff"));
            Mock<Light> greenLight = new Mock<Light>();
            List<string> greenCalls = new List<string>();
            greenLight.Setup(m => m.SwitchOn()).Callback(() => greenCalls.Add("SwitchOn"));
            greenLight.Setup(m => m.SwitchOff()).Callback(() => greenCalls.Add("SwitchOff"));
            int timeoutIndex = 0;
            int[] timeouts = new int[] { 120, 2 };
            Mock<OneShotTimer> oneShotTimer = new Mock<OneShotTimer>();
            oneShotTimer.Setup(m => m.Schedule(It.IsAny<int>(), It.IsAny<TimeoutReached>())).Callback(
                (int timeout, TimeoutReached timeoutReached) =>
                {
                    if (timeoutIndex < timeouts.Length)
                    {
                        Assert.AreEqual(timeout, timeouts[timeoutIndex]);
                        timeoutIndex++;
                        timeoutReached();
                    }

                });
            Sequencer sequencer = new Sequencer(redLight.Object, amberLight.Object, greenLight.Object, oneShotTimer.Object);
            sequencer.Run();
            Assert.AreEqual(redCalls[0], "SwitchOn");
            Assert.AreEqual(redCalls[1], "SwitchOff");
            Assert.AreEqual(redCalls.Count, 2);
            Assert.AreEqual(amberCalls[0], "SwitchOff");
            Assert.AreEqual(amberCalls[1], "SwitchOn");
            Assert.AreEqual(amberCalls[2], "SwitchOff");
            Assert.AreEqual(amberCalls.Count, 3);
            Assert.AreEqual(greenCalls[0], "SwitchOff");
            Assert.AreEqual(greenCalls[1], "SwitchOn");
            Assert.AreEqual(greenCalls.Count, 2);
        }

        [TestMethod]
        public void StopSequenceTest()
        {
            Mock<Light> redLight = new Mock<Light>();
            List<string> redCalls = new List<string>();
            redLight.Setup(m => m.SwitchOn()).Callback(() => redCalls.Add("SwitchOn"));
            redLight.Setup(m => m.SwitchOff()).Callback(() => redCalls.Add("SwitchOff"));
            Mock<Light> amberLight = new Mock<Light>();
            List<string> amberCalls = new List<string>();
            amberLight.Setup(m => m.SwitchOn()).Callback(() => amberCalls.Add("SwitchOn"));
            amberLight.Setup(m => m.SwitchOff()).Callback(() => amberCalls.Add("SwitchOff"));
            Mock<Light> greenLight = new Mock<Light>();
            List<string> greenCalls = new List<string>();
            greenLight.Setup(m => m.SwitchOn()).Callback(() => greenCalls.Add("SwitchOn"));
            greenLight.Setup(m => m.SwitchOff()).Callback(() => greenCalls.Add("SwitchOff"));
            int timeoutIndex = 0;
            int[] timeouts = new int[] { 120, 2, 120, 2 };
            Mock<OneShotTimer> oneShotTimer = new Mock<OneShotTimer>();
            oneShotTimer.Setup(m => m.Schedule(It.IsAny<int>(), It.IsAny<TimeoutReached>())).Callback(
                (int timeout, TimeoutReached timeoutReached) =>
                {
                    if (timeoutIndex < timeouts.Length)
                    {
                        Assert.AreEqual(timeout, timeouts[timeoutIndex]);
                        timeoutIndex++;
                        timeoutReached();
                    }

                });
            Sequencer sequencer = new Sequencer(redLight.Object, amberLight.Object, greenLight.Object, oneShotTimer.Object);
            sequencer.Run();
            Assert.AreEqual(redCalls[0], "SwitchOn");
            Assert.AreEqual(redCalls[1], "SwitchOff");
            Assert.AreEqual(redCalls[2], "SwitchOn");
            Assert.AreEqual(redCalls.Count, 3);
            Assert.AreEqual(amberCalls[0], "SwitchOff");
            Assert.AreEqual(amberCalls[1], "SwitchOn");
            Assert.AreEqual(amberCalls[2], "SwitchOff");
            Assert.AreEqual(amberCalls[3], "SwitchOn");
            Assert.AreEqual(amberCalls[4], "SwitchOff");
            Assert.AreEqual(amberCalls.Count, 5);
            Assert.AreEqual(greenCalls[0], "SwitchOff");
            Assert.AreEqual(greenCalls[1], "SwitchOn");
            Assert.AreEqual(greenCalls[2], "SwitchOff");
            Assert.AreEqual(greenCalls[3], "SwitchOff");
            Assert.AreEqual(greenCalls.Count, 4);
        }

        [TestMethod]
        public void PedestrianRequestSuccessfulTest()
        {
            Mock<Light> redLight = new Mock<Light>();
            List<string> redCalls = new List<string>();
            redLight.Setup(m => m.SwitchOn()).Callback(() => redCalls.Add("SwitchOn"));
            redLight.Setup(m => m.SwitchOff()).Callback(() => redCalls.Add("SwitchOff"));
            Mock<Light> amberLight = new Mock<Light>();
            List<string> amberCalls = new List<string>();
            amberLight.Setup(m => m.SwitchOn()).Callback(() => amberCalls.Add("SwitchOn"));
            amberLight.Setup(m => m.SwitchOff()).Callback(() => amberCalls.Add("SwitchOff"));
            Mock<Light> greenLight = new Mock<Light>();
            List<string> greenCalls = new List<string>();
            greenLight.Setup(m => m.SwitchOn()).Callback(() => greenCalls.Add("SwitchOn"));
            greenLight.Setup(m => m.SwitchOff()).Callback(() => greenCalls.Add("SwitchOff"));
            int timeoutIndex = 0;
            int[] timeouts = new int[] { 120, 2 };
            Mock<OneShotTimer> oneShotTimer = new Mock<OneShotTimer>();
            oneShotTimer.Setup(m => m.Schedule(It.IsAny<int>(), It.IsAny<TimeoutReached>())).Callback(
                (int timeout, TimeoutReached timeoutReached) =>
                {
                    if (timeoutIndex < timeouts.Length)
                    {
                        Assert.AreEqual(timeout, timeouts[timeoutIndex]);
                        timeoutIndex++;
                        timeoutReached();
                    }

                });
            Sequencer sequencer = new Sequencer(redLight.Object, amberLight.Object, greenLight.Object, oneShotTimer.Object);
            sequencer.Run();
            timeoutIndex = 0;
            timeouts = new int[] { 5, 2 };
            sequencer.PedestrianRequest();
            Assert.AreEqual(redCalls[0], "SwitchOn");
            Assert.AreEqual(redCalls[1], "SwitchOff");
            Assert.AreEqual(redCalls[2], "SwitchOn");
            Assert.AreEqual(redCalls.Count, 3);
            Assert.AreEqual(amberCalls[0], "SwitchOff");
            Assert.AreEqual(amberCalls[1], "SwitchOn");
            Assert.AreEqual(amberCalls[2], "SwitchOff");
            Assert.AreEqual(amberCalls[3], "SwitchOn");
            Assert.AreEqual(amberCalls[4], "SwitchOff");
            Assert.AreEqual(amberCalls.Count, 5);
            Assert.AreEqual(greenCalls[0], "SwitchOff");
            Assert.AreEqual(greenCalls[1], "SwitchOn");
            Assert.AreEqual(greenCalls[2], "SwitchOff");
            Assert.AreEqual(greenCalls[3], "SwitchOff");
            Assert.AreEqual(greenCalls.Count, 4);
        }

        [TestMethod]
        public void PedestrianRequestUnsuccessfulTest1()
        {
            Mock<Light> redLight = new Mock<Light>();
            List<string> redCalls = new List<string>();
            redLight.Setup(m => m.SwitchOn()).Callback(() => redCalls.Add("SwitchOn"));
            redLight.Setup(m => m.SwitchOff()).Callback(() => redCalls.Add("SwitchOff"));
            Mock<Light> amberLight = new Mock<Light>();
            List<string> amberCalls = new List<string>();
            amberLight.Setup(m => m.SwitchOn()).Callback(() => amberCalls.Add("SwitchOn"));
            amberLight.Setup(m => m.SwitchOff()).Callback(() => amberCalls.Add("SwitchOff"));
            Mock<Light> greenLight = new Mock<Light>();
            List<string> greenCalls = new List<string>();
            greenLight.Setup(m => m.SwitchOn()).Callback(() => greenCalls.Add("SwitchOn"));
            greenLight.Setup(m => m.SwitchOff()).Callback(() => greenCalls.Add("SwitchOff"));
            Mock<OneShotTimer> oneShotTimer = new Mock<OneShotTimer>();
            Sequencer sequencer = new Sequencer(redLight.Object, amberLight.Object, greenLight.Object, oneShotTimer.Object);
            sequencer.Run();
            int timeoutIndex = 0;
            int[] timeouts = new int[] { 5 };
            oneShotTimer.Setup(m => m.Schedule(It.IsAny<int>(), It.IsAny<TimeoutReached>())).Callback(
                (int timeout, TimeoutReached timeoutReached) =>
                {
                    if (timeoutIndex < timeouts.Length)
                    {
                        Assert.AreEqual(timeout, timeouts[timeoutIndex]);
                        timeoutIndex++;
                        timeoutReached();
                    }

                });
            sequencer.PedestrianRequest();
            Assert.AreEqual(timeoutIndex, 0);
            Assert.AreEqual(redCalls[0], "SwitchOn");
            Assert.AreEqual(redCalls.Count, 1);
            Assert.AreEqual(amberCalls[0], "SwitchOff");
            Assert.AreEqual(amberCalls.Count, 1);
            Assert.AreEqual(greenCalls[0], "SwitchOff");
            Assert.AreEqual(greenCalls.Count, 1);
        }

        [TestMethod]
        public void PedestrianRequestUnsuccessfulTest2()
        {
            Mock<Light> redLight = new Mock<Light>();
            List<string> redCalls = new List<string>();
            redLight.Setup(m => m.SwitchOn()).Callback(() => redCalls.Add("SwitchOn"));
            redLight.Setup(m => m.SwitchOff()).Callback(() => redCalls.Add("SwitchOff"));
            Mock<Light> amberLight = new Mock<Light>();
            List<string> amberCalls = new List<string>();
            amberLight.Setup(m => m.SwitchOn()).Callback(() => amberCalls.Add("SwitchOn"));
            amberLight.Setup(m => m.SwitchOff()).Callback(() => amberCalls.Add("SwitchOff"));
            Mock<Light> greenLight = new Mock<Light>();
            List<string> greenCalls = new List<string>();
            greenLight.Setup(m => m.SwitchOn()).Callback(() => greenCalls.Add("SwitchOn"));
            greenLight.Setup(m => m.SwitchOff()).Callback(() => greenCalls.Add("SwitchOff"));
            int timeoutIndex = 0;
            int[] timeouts = new int[] { 120, 2, 120 };
            Mock<OneShotTimer> oneShotTimer = new Mock<OneShotTimer>();
            oneShotTimer.Setup(m => m.Schedule(It.IsAny<int>(), It.IsAny<TimeoutReached>())).Callback(
                (int timeout, TimeoutReached timeoutReached) =>
                {
                    if (timeoutIndex < timeouts.Length)
                    {
                        Assert.AreEqual(timeout, timeouts[timeoutIndex]);
                        timeoutIndex++;
                        timeoutReached();
                    }

                });
            Sequencer sequencer = new Sequencer(redLight.Object, amberLight.Object, greenLight.Object, oneShotTimer.Object);
            sequencer.Run();
            sequencer.PedestrianRequest();
            Assert.AreEqual(timeoutIndex, 3);
            Assert.AreEqual(redCalls[0], "SwitchOn");
            Assert.AreEqual(redCalls[1], "SwitchOff");
            Assert.AreEqual(redCalls.Count, 2);
            Assert.AreEqual(amberCalls[0], "SwitchOff");
            Assert.AreEqual(amberCalls[1], "SwitchOn");
            Assert.AreEqual(amberCalls[2], "SwitchOff");
            Assert.AreEqual(amberCalls[3], "SwitchOn");
            Assert.AreEqual(amberCalls.Count, 4);
            Assert.AreEqual(greenCalls[0], "SwitchOff");
            Assert.AreEqual(greenCalls[1], "SwitchOn");
            Assert.AreEqual(greenCalls[2], "SwitchOff");
            Assert.AreEqual(greenCalls.Count, 3);
        }
    }

}
