﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TrafficLightsSequencer;

namespace TrafficLightsSequencerTests
{
    [TestClass]
    public class TrafficLightsSequencerGoodTests
    {
        public const int TwoMinutes = 120;
        public const int TwoSeconds = 2;
        public const int FiveSeconds = 5;
        private Mock<Light> redLight;
        private Mock<Light> amberLight;
        private Mock<Light> greenLight;
        private Verify verify;
        private Mock<OneShotTimer> oneShotTimer;
        private TimeoutInvoker timeoutInvoker;

        private Sequencer sequencer;

        [TestInitialize()]
        public void Initialize()
        {
            redLight = new Mock<Light>();
            amberLight = new Mock<Light>();
            greenLight = new Mock<Light>();
            oneShotTimer = new Mock<OneShotTimer>();
            timeoutInvoker = new TimeoutInvoker(oneShotTimer);
            verify = new Verify(redLight, amberLight, greenLight);

            sequencer = new Sequencer(redLight.Object, 
                amberLight.Object, 
                greenLight.Object, 
                oneShotTimer.Object);
        }

        private void RunToGoState()
        {
            timeoutInvoker.InvokeNumberOfTimes(2);
            sequencer.Run();
        }

        private void RunToPrepareToStopState()
        {
            timeoutInvoker.InvokeNumberOfTimes(3);
            sequencer.Run();
        }

        // This test is superceded by LightsChangedZeroTimes_Stop but has been left to demonstrate
        // how the tests evolved incrementally. Normally this should now be deleted.
        [TestMethod]
        public void LightsChangedZeroTimes_RedLightIsLit()
        {
            sequencer.Run();

            redLight.Verify(m => m.SwitchOn(), Times.Exactly(1));
        }

        // This test is superceded by LightsChangedZeroTimes_Stop but has been left to demonstrate
        // how the tests evolved incrementally. Normally this should now be deleted.
        [TestMethod]
        public void LightsChangedZeroTimes_AmberLightIsNotLit()
        {
            sequencer.Run();

            amberLight.Verify(m => m.SwitchOff(), Times.Exactly(1));
        }

        // This test is superceded by LightsChangedZeroTimes_Stop but has been left to demonstrate
        // how the tests evolved incrementally. Normally this should now be deleted.
        [TestMethod]
        public void LightsChangedZeroTimes_GreenLightIsNotLit()
        {
            sequencer.Run();

            greenLight.Verify(m => m.SwitchOff(), Times.Exactly(1));
        }

        [TestMethod]
        public void LightsChangedZeroTimes_Stop()
        {
            sequencer.Run();

            verify.StopState();
        }

        [TestMethod]
        public void LightsChangedZeroTimes_ChangeScheduledForTwoMinutes()
        {
            sequencer.Run();

            Assert.AreEqual(TwoMinutes, timeoutInvoker.Occurrence(1));
        }

        [TestMethod]
        public void LightsChangedOnce_PrepareToGo()
        {
            timeoutInvoker.InvokeNumberOfTimes(1);

            sequencer.Run();

            verify.PrepareToGoState();
        }

        [TestMethod]
        public void LightsChangedOnce_NextChangeScheduledForTwoSeconds()
        {
            timeoutInvoker.InvokeNumberOfTimes(1);

            sequencer.Run();

            Assert.AreEqual(TwoSeconds, timeoutInvoker.Occurrence(2));
        }

        [TestMethod]
        public void LightsChangedTwice_GoState()
        {
            timeoutInvoker.InvokeNumberOfTimes(2);

            sequencer.Run();

            verify.GoState();
        }

        [TestMethod]
        public void LightsChangedTwice_NextChangeScheduledForTwoMinutes()
        {
            timeoutInvoker.InvokeNumberOfTimes(2);

            sequencer.Run();

            Assert.AreEqual(TwoMinutes, timeoutInvoker.Occurrence(3));
        }

        [TestMethod]
        public void LightsChangedThreeTimes_PrepareToStop()
        {
            timeoutInvoker.InvokeNumberOfTimes(3);

            sequencer.Run();

            verify.PrepareToStopState();
        }

        [TestMethod]
        public void LightsChangedThreeTimes_NextChangeScheduledForTwoSeconds()
        {
            timeoutInvoker.InvokeNumberOfTimes(3);

            sequencer.Run();

            Assert.AreEqual(TwoSeconds, timeoutInvoker.Occurrence(4));
        }

        [TestMethod]
        public void LightsChangedFourTimes_Stop()
        {
            timeoutInvoker.InvokeNumberOfTimes(4);

            sequencer.Run();

            verify.StopState();
        }

        [TestMethod]
        public void LightsChangedFiveTimes_NextChangeScheduledForTwoMinutes()
        {
            timeoutInvoker.InvokeNumberOfTimes(4);

            sequencer.Run();

            Assert.AreEqual(TwoMinutes, timeoutInvoker.Occurrence(5));
        }

        [TestMethod]
        public void InGoState_PedestrianRequest_NextChangeScheduledForFiveSeconds()
        {
            RunToGoState();

            sequencer.PedestrianRequest();

            Assert.AreEqual(FiveSeconds, timeoutInvoker.Occurrence(4));
        }

        [TestMethod]
        public void InGoState_PedestrianRequestAndOneLightChange_PrepareToStop()
        {
            RunToGoState();
            timeoutInvoker.InvokeNumberOfTimes(1);

            sequencer.PedestrianRequest();

            verify.PrepareToStopState();
        }

        [TestMethod]
        public void InGoState_PedestrianRequestAndOneLightChange_NextChangeScheduledForTwoSeconds()
        {
            RunToGoState();
            timeoutInvoker.InvokeNumberOfTimes(1);

            sequencer.PedestrianRequest();

            Assert.AreEqual(TwoSeconds, timeoutInvoker.Occurrence(5));
        }

        [TestMethod]
        public void InGoState_PedestrianRequestAndTwoLightChanges_Stop()
        {
            RunToGoState();
            timeoutInvoker.InvokeNumberOfTimes(2);

            sequencer.PedestrianRequest();

            verify.StopState();
        }

        [TestMethod]
        public void InStopState_PedestrianRequest_NoScheduledPedestrianStop()
        {
            sequencer.Run();
            sequencer.PedestrianRequest();

            Assert.AreNotEqual(FiveSeconds, timeoutInvoker.LastOccurrence);
        }

        [TestMethod]
        public void InPrepareToStopState_PedestrianRequest_NoScheduledPedestrianStop()
        {
            RunToPrepareToStopState();

            sequencer.PedestrianRequest();

            Assert.AreNotEqual(FiveSeconds, timeoutInvoker.LastOccurrence);
        }
    }
}
