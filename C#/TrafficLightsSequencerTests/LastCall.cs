﻿using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrafficLightsSequencer;

namespace TrafficLightsSequencerTests
{
    public class LastCall
    {
        public delegate void LightAction();
        private Mock<Light> light;
        LightAction lastCall = null;

        public LastCall(Mock<Light> newLight)
        {
            light = newLight;

            light.Setup(m => m.SwitchOn()).Callback(() => lastCall = light.Object.SwitchOn);
            light.Setup(m => m.SwitchOff()).Callback(() => lastCall = light.Object.SwitchOff);
        }

        public void VerifySwitchedOn()
        {
            Assert.AreEqual(lastCall, light.Object.SwitchOn, "Expected to be switched on but was switched off");
        }

        public void VerifySwitchedOff()
        {
            Assert.AreEqual(lastCall, light.Object.SwitchOff, "Expected to be switched off but was switched on");
        }
    }
}
