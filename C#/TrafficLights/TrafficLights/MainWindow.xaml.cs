﻿using System.Windows;
using System.Windows.Media;
using TrafficLightsSequencer;

namespace TrafficLights
{
    public partial class MainWindow : Window
    {
        public TrafficLight RedLight { get; set; }
        public TrafficLight AmberLight { get; set; }
        public TrafficLight GreenLight { get; set; }
        private Sequencer sequencer;
        private OneShotTimer oneShotTimer;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            RedLight = new TrafficLight(Colors.DarkRed, Colors.Red);
            AmberLight = new TrafficLight(Colors.DarkGoldenrod, Colors.Yellow);
            GreenLight = new TrafficLight(Colors.DarkGreen, Colors.Lime);
            oneShotTimer = new AsynchronousOneShotTimer();
            sequencer = new Sequencer(RedLight, AmberLight, GreenLight, oneShotTimer);
            sequencer.Run();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            sequencer.PedestrianRequest();
        }
    }
}
