﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Media;
using TrafficLightsSequencer;

namespace TrafficLights
{
    public class TrafficLight : INotifyPropertyChanged, Light
    {
        private Color onColour;
        private Color offColour;

        public TrafficLight(Color newOffColour, Color newOnColour)
        {
            onColour = newOnColour;
            offColour = newOffColour;
            Color = offColour;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        private Color _colour;
        public Color Color
        {
            get { return _colour; }
            set
            {
                _colour = value;
                NotifyPropertyChanged("Color");
            }
        }

        public void SwitchOff()
        {
            Color = offColour;
        }

        public void SwitchOn()
        {
            Color = onColour;
        }

    }
}
