﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficLightsSequencer
{
    public interface Light
    {
        void SwitchOn();
        void SwitchOff();
    }
}
