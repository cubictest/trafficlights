﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficLightsSequencer;

namespace TrafficLightsSequencer
{
    public class Sequencer
    {
        private const int BetweenSequenceGapInSeconds = 120;
        private const int SequenceChangeGapInSeconds = 2;
        private const int PedestrianRequestWaitInSeconds = 5;
        private Light redLight;
        private Light amberLight;
        private Light greenLight;
        private OneShotTimer oneShotTimer;
        private bool inGoState;

        public Sequencer(Light newRedLight,
            Light newAmberLight,
            Light newGreenLight,
            OneShotTimer newOneShotTimer)
        {
            redLight = newRedLight;
            amberLight = newAmberLight;
            greenLight = newGreenLight;
            oneShotTimer = newOneShotTimer;
            inGoState = false;
        }

        public void Run()
        {
            Stop();
        }

        public void PedestrianRequest()
        {
            if (inGoState)
            {
                oneShotTimer.Schedule(PedestrianRequestWaitInSeconds, PrepareToStop);
            }
        }

        private void Stop()
        {
            redLight.SwitchOn();
            amberLight.SwitchOff();
            greenLight.SwitchOff();

            oneShotTimer.Schedule(BetweenSequenceGapInSeconds, PrepareToGo);
        }

        private void PrepareToGo()
        {
            amberLight.SwitchOn();

            oneShotTimer.Schedule(SequenceChangeGapInSeconds, Go);
        }

        private void Go()
        {
            inGoState = true;
            redLight.SwitchOff();
            amberLight.SwitchOff();
            greenLight.SwitchOn();

            oneShotTimer.Schedule(BetweenSequenceGapInSeconds, PrepareToStop);
        }

        private void PrepareToStop()
        {
            inGoState = false;
            amberLight.SwitchOn();
            greenLight.SwitchOff();

            oneShotTimer.Schedule(SequenceChangeGapInSeconds, Stop);
        }
    }
}
