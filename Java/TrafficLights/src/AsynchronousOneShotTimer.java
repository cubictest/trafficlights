import java.util.Timer;
import java.util.TimerTask;

class Callback extends TimerTask
{
    private Runnable callbackFunction;

    public Callback(Runnable newCallbackFunction)
    {
        callbackFunction = newCallbackFunction;
    }

    @Override
    public void run()
    {
        callbackFunction.run();
    }
}

public class AsynchronousOneShotTimer implements OneShotTimer {

    public static final int MILLISECONDS_IN_SECOND = 1000;
    Timer timer;

    private Callback callback;
    @Override
    public void Schedule(int timeoutInSeconds, Runnable callbackFunction)
    {
        timer = new Timer();
        callback = new Callback(callbackFunction);
        timer.schedule(callback, timeoutInSeconds * MILLISECONDS_IN_SECOND);
    }
}
