import javax.swing.*;
import java.awt.Color;

public class UILight implements Light
{
    private JButton button;
    private Color onColour;
    private Color offColour;

    public UILight(JButton newButton, Color newOnColour, Color newOffColour)
    {
        button = newButton;
        onColour = newOnColour;
        offColour = newOffColour;
    }

    @Override
    public void SwitchOn() {
        button.setBackground(onColour);
    }

    @Override
    public void SwitchOff() {
        button.setBackground(offColour);
    }
}
