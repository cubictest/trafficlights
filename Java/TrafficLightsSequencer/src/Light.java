public interface Light
{
    void SwitchOn();
    void SwitchOff();
}
