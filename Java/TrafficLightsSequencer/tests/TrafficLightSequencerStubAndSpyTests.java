import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TrafficLightSequencerStubAndSpyTests {

    final int TWO_MINUTES = 120;
    final int TWO_MINUTES_TWO_SECOND = 122;
    final int FOUR_MINUTES_AND_TWO_SECOND = 242;
    final int FOUR_MINUTES_AND_FOUR_SECONDS = 244;
    private LightSpy redLight;
    private LightSpy amberLight;
    private LightSpy greenLight;
    private OneShotTimerStub deterministicOneShotTimer;
    private TrafficLightSequencer trafficLights;

    @Before
    public void setUp() throws Exception {
        redLight = new LightSpy();
        greenLight = new LightSpy();
        amberLight = new LightSpy();
        deterministicOneShotTimer = new OneShotTimerStub();
        trafficLights = new TrafficLightSequencer(redLight, amberLight, greenLight, deterministicOneShotTimer);
    }

    private void AssertRedLightIsLit() {
        assertEquals(true, redLight.IsLit());
        assertEquals(false, amberLight.IsLit());
        assertEquals(false, greenLight.IsLit());
    }

    private void AssertAmberLightIsLit() {
        assertEquals(false, redLight.IsLit());
        assertEquals(true, amberLight.IsLit());
        assertEquals(false, greenLight.IsLit());
    }

    private void AssertGreenLightIsLit() {
        assertEquals(false, redLight.IsLit());
        assertEquals(false, amberLight.IsLit());
        assertEquals(true, greenLight.IsLit());
    }

    private void AssertRedAndAmberLightsAreLit() {
        assertEquals(true, redLight.IsLit());
        assertEquals(true, amberLight.IsLit());
        assertEquals(false, greenLight.IsLit());
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Run_ZeroSecondsElapsed_RedLightIsLit() throws Exception
    {
        trafficLights.Run();

        assertEquals(true, redLight.IsLit());
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Run_ZeroSecondsElapsed_AmberLightIsNotLit() throws Exception
    {
        trafficLights.Run();

        assertEquals(false, amberLight.IsLit());
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Run_ZeroSecondsElapsed_GreenLightIsNotLit() throws Exception {

        trafficLights.Run();

        assertEquals(false, greenLight.IsLit());
    }

    @Test
    public void Run_ZeroSecondsElapsed_Stop() throws Exception
    {
        trafficLights.Run();

        AssertRedLightIsLit();
    }

    @Test
    public void Run_TwoMinutesElapsed_PrepareToGo() throws Exception
    {
        deterministicOneShotTimer.ElapseTime(TWO_MINUTES);

        trafficLights.Run();

        AssertRedAndAmberLightsAreLit();
    }

    @Test
    public void Run_TwoMinutesTwoSecondsElapsed_Go() throws Exception
    {
        deterministicOneShotTimer.ElapseTime(TWO_MINUTES_TWO_SECOND);

        trafficLights.Run();

        AssertGreenLightIsLit();
    }

    @Test
    public void Run_FourMinutesTwoSecondsElapsed_PrepareToStop() throws Exception
    {
        deterministicOneShotTimer.ElapseTime(FOUR_MINUTES_AND_TWO_SECOND);

        trafficLights.Run();

        AssertAmberLightIsLit();
    }

    @Test
    public void Run_FourMinutesFourSecondsElapsed_Stop() throws Exception
    {
        deterministicOneShotTimer.ElapseTime(FOUR_MINUTES_AND_FOUR_SECONDS);

        trafficLights.Run();

        AssertRedLightIsLit();
    }
}
