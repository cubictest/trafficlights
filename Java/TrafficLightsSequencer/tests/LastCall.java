import org.mockito.exceptions.base.MockitoException;
import org.mockito.internal.invocation.InvocationMatcher;
import org.mockito.internal.reporting.SmartPrinter;
import org.mockito.internal.verification.api.VerificationData;
import org.mockito.internal.verification.argumentmatching.ArgumentMatchingTool;
import org.mockito.invocation.Invocation;
import org.mockito.invocation.Location;
import org.mockito.verification.VerificationMode;

import java.util.List;

public class LastCall implements VerificationMode {

    public static LastCall lastCall() {

        return new LastCall();
    }

    public void verify(VerificationData data) {

        List<Invocation> invocations = data.getAllInvocations();

        InvocationMatcher matcher = data.getWanted();

        final Invocation invocation = invocations.get(invocations.size() - 1);

        if (!matcher.matches(invocation))
            throwUnexpectedLastCall(matcher, invocation);
    }

    private void throwUnexpectedLastCall(InvocationMatcher wanted, Invocation invocation) {

        final Integer[] indicesOfSimilarMatchingArguments =
                new ArgumentMatchingTool()
                        .getSuspiciouslyNotMatchingArgsIndexes(wanted.getMatchers(),
                                invocation.getArguments());

        final SmartPrinter smartPrinter = new SmartPrinter(wanted, invocation,
                indicesOfSimilarMatchingArguments);

        throw new MockitoException(
                argumentsAreDifferentMessage(smartPrinter.getWanted(), smartPrinter.getActual(), invocation.getLocation()));
    }

    private String argumentsAreDifferentMessage(String expected, String actual, Location actualLocation) {
        return "Expected last call for mock to be " +
                expected +
                " Actual call was " +
                actual +
                actualLocation;
    }

}
