import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class TrafficLightSequencerMockTests {

    final int TWO_SECONDS = 2;
    final int TWO_MINUTES = 120;
    final int FIVE_SECONDS = 5;
    private Light redLight;
    private Light amberLight;
    private Light greenLight;
    private OneShotTimer oneShotTimer;
    private TrafficLightSequencer trafficLights;

    @Before
    public void setUp() throws Exception {

        redLight = mock(Light.class);
        greenLight = mock(Light.class);
        amberLight = mock(Light.class);
        oneShotTimer = mock(OneShotTimer.class);
        trafficLights = new TrafficLightSequencer(redLight, amberLight, greenLight, oneShotTimer);
    }

    private void VerifyScheduledForDurationAndInvokeCallback(int durationInSeconds)
    {
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);

        verify(oneShotTimer, atLeastOnce()).Schedule(eq(durationInSeconds), captor.capture());

        captor.getValue().run();
    }

    private void VerifyNextTimeoutDurationIs(int durationInSeconds)
    {
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);

        verify(oneShotTimer, atLeastOnce()).Schedule(eq(durationInSeconds), captor.capture());
    }

    private void RunToNextTimeout()
    {
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);

        verify(oneShotTimer, atLeastOnce()).Schedule(Mockito.any(Integer.class), captor.capture());

        captor.getValue().run();
    }

    private void AssertLightsInStopState()
    {
        verify(redLight, LastCall.lastCall()).SwitchOn();
        verify(amberLight, LastCall.lastCall()).SwitchOff();
        verify(greenLight, LastCall.lastCall()).SwitchOff();
    }

    private void AssertLightsInPrepareToStopState()
    {
        verify(redLight, LastCall.lastCall()).SwitchOff();
        verify(amberLight, LastCall.lastCall()).SwitchOn();
        verify(greenLight, LastCall.lastCall()).SwitchOff();
    }

    private void AssertLightsInGoState()
    {
        verify(redLight, LastCall.lastCall()).SwitchOff();
        verify(amberLight, LastCall.lastCall()).SwitchOff();
        verify(greenLight, LastCall.lastCall()).SwitchOn();
    }

    private void AssertLightsInPrepareToGoState()
    {
        verify(redLight, LastCall.lastCall()).SwitchOn();
        verify(amberLight, LastCall.lastCall()).SwitchOn();
        verify(greenLight, LastCall.lastCall()).SwitchOff();
    }

    private void AdvanceToStopState() {
        trafficLights.Run();
    }

    private void AdvanceToPrepareToGoState() {
        trafficLights.Run();
        RunToNextTimeout();
    }


    private void AdvanceToGoState() {
        trafficLights.Run();
        RunToNextTimeout();
        RunToNextTimeout();
    }

    private void AdvanceToPrepareToStopState() {
        trafficLights.Run();
        RunToNextTimeout();
        RunToNextTimeout();
        RunToNextTimeout();
    }

    private void PrepareToStopAfterPedestrianRequest() {
        AdvanceToGoState();
        trafficLights.PedestrianRequest();
        RunToNextTimeout();
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Initialised_RedLightIsLit() throws Exception
    {
        trafficLights.Run();

        verify(redLight, only()).SwitchOn();
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Initialised_AmberLightIsNotLit() throws Exception
    {
        trafficLights.Run();

        verify(amberLight, only()).SwitchOff();
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Initialised_GreenLightIsNotLit() throws Exception {

        trafficLights.Run();

        verify(greenLight, only()).SwitchOff();
    }

    @Test
    public void Initialised_InStopState() throws Exception
    {
        trafficLights.Run();

        AssertLightsInStopState();
    }

    @Test
    public void StopState_NextTimeoutInTwoMinutes() throws Exception
    {
        AdvanceToStopState();

        VerifyNextTimeoutDurationIs(TWO_MINUTES);
    }

    @Test
    public void PrepareToGoState_NextTimeoutInTwoSeconds() throws Exception
    {
        AdvanceToPrepareToGoState();

        VerifyNextTimeoutDurationIs(TWO_SECONDS);
    }

    @Test
    public void PrepareToGoState_TimesOut_GoState() throws Exception
    {
        AdvanceToPrepareToGoState();

        RunToNextTimeout();

        AssertLightsInGoState();
    }

    @Test
    public void GoState_NextTimeoutInTwoMinutes() throws Exception
    {
        AdvanceToGoState();

        VerifyNextTimeoutDurationIs(TWO_MINUTES);
    }

    @Test
    public void GoState_TimesOut_PrepareToStopState() throws Exception
    {
        AdvanceToGoState();

        RunToNextTimeout();

        AssertLightsInPrepareToStopState();
    }

    @Test
    public void PrepareToStopState_NextTimeoutInTwoSeconds() throws Exception
    {
        AdvanceToPrepareToStopState();

        VerifyNextTimeoutDurationIs(TWO_SECONDS);
    }

    @Test
    public void PrepareToStopState_TimesOut_StopState() throws Exception
    {
        AdvanceToPrepareToStopState();

        RunToNextTimeout();

        AssertLightsInStopState();
    }

    @Test
    public void GoState_PedestrianRequest_NextTimeoutInFiveSeconds() throws Exception
    {
        AdvanceToGoState();

        trafficLights.PedestrianRequest();
        VerifyNextTimeoutDurationIs(FIVE_SECONDS);
    }

    @Test
    public void GoState_PedestrianRequestThenTimesOut_PrepareToStop() throws Exception
    {
        AdvanceToGoState();

        trafficLights.PedestrianRequest();
        RunToNextTimeout();

        AssertLightsInPrepareToStopState();
    }

    @Test
    public void PrepareToStopAfterPedestrianRequest_NextTimeoutInTwoSeconds() throws Exception
    {
        PrepareToStopAfterPedestrianRequest();

        VerifyNextTimeoutDurationIs(TWO_SECONDS);
    }

    @Test
    public void PrepareToStopAfterPedestrianRequest_TimesOut_StopState() throws Exception
    {
        PrepareToStopAfterPedestrianRequest();

        RunToNextTimeout();

        AssertLightsInStopState();
    }

    @Test
    public void PrepareToStopState_PedestrianRequest_NoScheduledPedestrianStop() throws Exception
    {
        AdvanceToPrepareToStopState();

        trafficLights.PedestrianRequest();

        VerifyNextTimeoutDurationIs(TWO_SECONDS);
    }

    @Test
    public void StopState_PedestrianRequest_NoScheduledPedestrianStop() throws Exception
    {
        AdvanceToStopState();

        trafficLights.PedestrianRequest();

        VerifyNextTimeoutDurationIs(TWO_MINUTES);
    }

}
