public class OneShotTimerStub implements OneShotTimer
{
    int elapseTimeInSeconds;

    public void Schedule(int newTimeoutInSeconds, Runnable callbackFunction)
    {
        elapseTimeInSeconds -= newTimeoutInSeconds;
        if (elapseTimeInSeconds >= 0) {
            callbackFunction.run();
        }
    }

    public void ElapseTime(int newElapseTimeInSeconds)
    {
        elapseTimeInSeconds = newElapseTimeInSeconds;
    }
}
